#!/bin/bash

export TZ=Asia/Shanghai

# export SITE_DOMAIN="mysite.com"
# export SITE_NAME="mysite"
# export DBNAME="v2b-demo"
# export DBUSER="root"
# export DBPASSWD="root-password"
# export ADMINUSER="admin@admin.com"
source /root/.bashrc

mkdir -p /root/.caddy

# Define variables
ZIP_URL="https://gitlab.com/vampire998/tools/-/raw/main/v2b/v2b-docker-demo.zip?ref_type=heads&inline=false"
BASE_DIR="/opt/apps"
APP_DIR="v2b-docker-demo"
TMP_DIR="/tmp"
REPOSITORY_NAME="v2board-docker"
LOG_FILE="$BASE_DIR/install.log"

# Define file paths relative to $BASE_DIR
CADDY_DIR=".caddy"
CADDY_CONF="caddy.conf"
CHECK_DOCKER_LOGS="check-docker-logs.sh"
CRONTABS_CONF="crontabs.conf"
DOCKER_COMPOSE_YAML="docker-compose.yaml"
MYSQL_DIR="mysql"
REDIS_DIR="redis"
SUPERVISORD_CONF="supervisord.conf"

CONTAINER_NAME="v2b-www-demo"

# Function to log messages
log() {
    local message="$1"
    echo "$(date +"%Y-%m-%d %H:%M:%S"): $message" | tee -a "$LOG_FILE"
}

# Create log directory if it doesn't exist
mkdir -p "$LOG_DIR"

# Log the script start time
log "Script started."

# Download ZIP file
log "Downloading the ZIP file..."
wget --inet4-only -O "$TMP_DIR/v2b-docker-demo.zip" "$ZIP_URL" 2>&1 | tee -a "$LOG_FILE"

# Check if the download was successful
if [ $? -eq 0 ]; then
    log "ZIP file downloaded successfully."
    
    # Unzip the file
    log "Extracting the ZIP file to $TMP_DIR..."
    unzip -o "$TMP_DIR/v2b-docker-demo.zip" -d "$TMP_DIR" 2>&1 | tee -a "$LOG_FILE"
    
    # Check if the unzip was successful
    if [ $? -eq 0 ]; then
        log "ZIP file extracted successfully to $TMP_DIR."
        
        # Change directory to $BASE_DIR
        cd "$BASE_DIR"

        # Clone the GitHub repository and perform other commands
        log "Cloning the GitHub repository and performing other commands..."
        git clone https://github.com/v2board/v2board-docker.git 2>&1 | tee -a "$LOG_FILE"
        cd $REPOSITORY_NAME
        git submodule update --init 2>&1 | tee -a "$LOG_FILE"
        echo '  branch = dev' >> .gitmodules
        git submodule update --remote 2>&1 | tee -a "$LOG_FILE"
        cd ..
        
        # Copy necessary files to v2board-docker directory and overwrite existing files
        log "Copying necessary files to v2board-docker directory..."
        cp -rf "$TMP_DIR/$APP_DIR/$CADDY_DIR" "$BASE_DIR/$REPOSITORY_NAME"
        cp -rf "$TMP_DIR/$APP_DIR/$CADDY_CONF" "$BASE_DIR/$REPOSITORY_NAME"
        cp -rf "$TMP_DIR/$APP_DIR/$CHECK_DOCKER_LOGS" "$BASE_DIR/$REPOSITORY_NAME"
        cp -rf "$TMP_DIR/$APP_DIR/$CRONTABS_CONF" "$BASE_DIR/$REPOSITORY_NAME"
        cp -rf "$TMP_DIR/$APP_DIR/$DOCKER_COMPOSE_YAML" "$BASE_DIR/$REPOSITORY_NAME"
        cp -rf "$TMP_DIR/$APP_DIR/$MYSQL_DIR" "$BASE_DIR/$REPOSITORY_NAME"
        cp -rf "$TMP_DIR/$APP_DIR/$REDIS_DIR" "$BASE_DIR/$REPOSITORY_NAME"
        cp -rf "$TMP_DIR/$APP_DIR/$SUPERVISORD_CONF" "$BASE_DIR/$REPOSITORY_NAME"

        sed -i "s/your-domain/$SITE_DOMAIN/g" "$BASE_DIR/$REPOSITORY_NAME/$CADDY_CONF"
        sed -i "s/-demo/-$SITE_NAME/g" "$BASE_DIR/$REPOSITORY_NAME/$DOCKER_COMPOSE_YAML"
        sed -i "s/MYSQL_ROOT_PASSWORD: your-database-password/MYSQL_ROOT_PASSWORD: $DBPASSWD/g" "$BASE_DIR/$REPOSITORY_NAME/$DOCKER_COMPOSE_YAML"
        
        log "All steps completed successfully."
    else
        log "Error: Failed to extract the ZIP file."
    fi
else
    log "Error: Failed to download the ZIP file."
fi

# Log the script end time
log "Script finished."
