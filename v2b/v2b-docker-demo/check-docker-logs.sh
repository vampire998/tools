#!/bin/sh

echo "======== start clean========"
df -h

logs=$(find /var/lib/docker/containers/ -name *-json.log)

for log in $logs
        do
                echo "clean logs : $log"
                ls -lh $log
                cat /dev/null > $log
                ls -lh $log
        done

df -h

#docker volume prune
echo "y" | docker volume prune
df -h

echo "======== end clean ========"
